// console.log("Hello World");

let number = Number(prompt("Give me a number."))

console.log("The number you provided is "+number+".")

for(let count = number; count >= 0; count--) {
	if(count % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(count % 5 === 0) {
		console.log(count);
		continue;
	}

	if(count <= 51) {
		// will stop the iteration of the loop once the condition is true.
		console.log("The current number is at 50. Terminating the loop.")
		break;
	}
}

